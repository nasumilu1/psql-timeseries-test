package io.nasumilu;

import java.sql.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * A simple app which utilizes Java 21 Virtual Threads to concurrently insert timeseries test data into a
 * postgresql/postgis database.
 * <p>
 * @see <a href="https://hub.docker.com/r/postgis/postgis">postgis container</a>
 */
public class App {


    // change to connect to a running postgresql server
    private static final String url = "jdbc:postgresql://localhost:5432/<DB_NAME>?user=<DB_USERNAME>&password=<DB_PASSWORD>";

    private record InsertFeatureTask(Connection connection, int numFeatures) implements Callable<List<Long>> {

        @Override
        public List<Long> call() throws Exception {
            var randomDeg = new Random();
            IntStream.range(0, numFeatures)
                    // ewkt from two random values x-coordinate between -180 and 180, y-coordinate between -90 and 90
                    .mapToObj(i -> String.format("srid=4326;Point (%.2f %.2f)", randomDeg.nextDouble(-180, 181), randomDeg.nextDouble(-90, 91)))
                    .forEach(ewkt -> {
                        // insert the event's location by casting the ewkt to a geometry
                        // same as `cast(? as geometry)
                        try (var stmt = connection.prepareStatement("insert into event_location (geom) values (?::geometry)")) {
                            stmt.setString(1, ewkt);
                            stmt.executeUpdate();
                        } catch (SQLException e) {
                            System.out.println(e.getMessage());
                            throw new RuntimeException(e);
                        }
                    });
            // select all feature id's inserted to return
            var ids = new LinkedList<Long>();
            try (var stmt = connection.prepareStatement("select id from event_location")) {
                var rs = stmt.executeQuery();
                while (rs.next()) {
                    ids.add(rs.getLong("id"));
                }
            }
            // list of long feature id value
            return ids;
        }
    }

    private record InsertTimeSeries(Connection connection, Long featureId) implements Callable<Void> {

        @Override
        public Void call() {
            // the number of time series records per feature (default: 1000)
            var numTestRecords = 1000;
            // the start date for the time series records (default: 1900-01-01)
            var startDate = LocalDate.of(1900, 1, 1);

            // loop to create and insert the time series record, incrementing by 1 day per iteration
            IntStream.range(0, numTestRecords).forEach(i -> {
                var period = Period.ofDays(i);
                try (var stmt = connection.prepareStatement(
                        """
                                insert into test_data (event_location, event_start_date, series_interval, series_values)
                                values (?, ?, ?::interval, ?)
                                """)) {

                    stmt.setLong(1, featureId);
                    stmt.setDate(2, Date.valueOf(startDate.plus(period)));
                    stmt.setString(3, "PT15M"); // default 15 minute duration
                    // create the random time series data.
                    // default min value -1000, max value 1000
                    // data points per time series between 48 and 96
                    stmt.setArray(4, connection.createArrayOf(
                            "numeric",
                            new Random().doubles(-1000, 1000)
                                    .limit(new Random().nextInt(48, 97))
                                    .boxed()
                                    .toArray()
                    ));
                    stmt.executeUpdate();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                    throw new RuntimeException(e);
                }
            });
            return null;
        }

    }

    public static void main(String[] args) throws SQLException {
        // auto-closable connection
        try (var connection = DriverManager.getConnection(url)) {

            // auto-closable ExecutorService
            try (var exe = Executors.newVirtualThreadPerTaskExecutor()) {

                // insert the features and wait for the list of fid values. (default: 750)
                final var features = exe.submit(new InsertFeatureTask(connection, 750));

                // iterate over each of the fid values and spawn a virtual thread to insert the time series data and
                // collecting the Future into an immutable list.
                final var timeSeries = features.get().stream()
                        .map(featureId -> new InsertTimeSeries(connection, featureId))
                        .map(exe::submit)
                        .toList();

                // quick one off progress bar for the terminal, so long as any one thread is not done
                var i = 1;
                while(timeSeries.stream().anyMatch(f -> !f.isDone())) {
                    System.out.printf("[%-20s]\r", "#".repeat(i));
                    i = i < 20 ? i + 1 : 1;
                    // main thread so the progress has time to show correct for us mortals.
                    Thread.sleep(Duration.ofMillis(500));
                }

            } catch (ExecutionException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
