-- counts the total data points store in `test_data.series_values`.
select count(*)
from (select unnest(series_values) from test_data) s;

-- verify the test data
select t.event_location,
       count(*)                                      total_recrods,
       min(array_length(series_values, 1))           min_series_length,
       max(array_length(series_values, 1))           max_series_length,
       floor(avg(array_length(series_values, 1)))    avg_series_length,
       min(event_start_date)                         min_start_date,
       max(event_start_date)                         max_start_date,
       max(event_start_date) - min(event_start_date) num_days
from test_data t
group by t.event_location;


-- Gets the number of records per event location (point feature)
-- by default should be 1000 per feature
select event_location,
       count(*) total_records
from test_data
group by event_location;

-- basic CTE query to inflate the time series for features intersection an extent.
-- a point feature's location is randomly added so results shall vary
with series_data as (select el.id,
                            el.geom,
                            generate_series(
                                    td.event_start_date,
                                    td.event_start_date + (td.series_interval * array_length(td.series_values, 1)) -
                                    td.series_interval, td.series_interval
                            )                     series_date_time,
                            unnest(series_values) series_value
                     from test_data td
                              join event_location el on td.event_location = el.id)
select id,
       st_astext(geom) wkt,
       series_date_time,
       series_value
from series_data
where st_intersects(geom, st_makeenvelope(-128.81, 23.63, -55.51, 55.32, 4326))
order by id, series_date_time;

