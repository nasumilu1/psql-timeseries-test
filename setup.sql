-- setup sql for test database with time series data store in an array
drop table if exists event_location cascade;
create table if not exists event_location
(
    id   bigserial primary key,
    geom geometry(Point, 4326) not null
);

create index event_location_geom_idx
    on event_location using gist (geom);

drop table if exists test_data cascade;

create table if not exists test_data
(
    id               bigserial primary key,
    event_location   bigint    not null references event_location (id),
    event_start_date date      not null,
    series_interval  interval  not null,
    series_values    numeric[] not null
);

create index test_data_event_start_date_idx
    on test_data (event_start_date);